﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Service.OpenDoc;

namespace Dyd.BaseService.ServiceCenter.Demo.Service
{
    /*
    * 服务仅支持的C#类型:bool;byte;int;int16(short);long;double;string;byte[];List<>;Dictionary<,>;void;class;
    * 非支持类型的服务在本地编译阶段,将报错。所以发布服务前仔细检查下服务及实体的类型。
    * 
    * 注意:公开的自定义实体必须包含[Doc]注释文档属性,否则服务或者方法不会解析成协议。
    */

    [EntityDoc("实体1", "这个实体用来示范传入参数")]
    public class MyEntity1
    {
        [PropertyDoc("实体参数1", "用来示范参数1")]
        public string p1 { get; set; }
    }

    [EntityDoc("测试实体2", "我的测试实体2")]
    public class MyEntity2
    {
        [PropertyDoc("List", "我的测试实体参数2")]
        public List<int> P2 { get; set; }
        [PropertyDoc("实体参数2", "我的测试实体参数3")]
        public List<byte> P3 { get; set; }
        [PropertyDoc("实体参数2", "我的测试实体参数3")]
        public Dictionary<string,string> P4 { get; set; }
        [PropertyDoc("实体参数5", "我的测试实体参数5")]
        public bool P5 { get; set; }
        [PropertyDoc("实体参数6", "我的测试实体参数6")]
        public long P6 { get; set; }
        [PropertyDoc("实体参数6", "我的测试实体参数6")]
        public short P7 { get; set; }
        [PropertyDoc("实体参数6", "我的测试实体参数6")]
        public Int16 P8 { get; set; }
        [PropertyDoc("实体参数6", "我的测试实体参数6")]
        public byte[] P9 { get; set; }
        [PropertyDoc("实体参数6", "我的测试实体参数6")]
        public string P10 { get; set; }
    }
}

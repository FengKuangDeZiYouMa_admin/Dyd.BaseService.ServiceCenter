﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Domain
{
    public static class DataConfig
    {
        public static string ServiceCenterConnectString = ConfigurationManager.AppSettings["ServiceCenterConnectString"];

        public static string ServiceCenterReportConnectString = ConfigurationManager.AppSettings["ServiceCenterReportConnectString"];
    }
}

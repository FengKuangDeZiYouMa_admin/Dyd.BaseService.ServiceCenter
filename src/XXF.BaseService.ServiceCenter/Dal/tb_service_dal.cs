using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ServiceCenter.Model;

namespace XXF.BaseService.ServiceCenter.Dal
{
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
	public partial class tb_service_dal
    {
        public virtual bool UpdateLastUpdateTime(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
            };
            Par.Add(new ProcedureParameter("@id", id));

            int rev = PubConn.ExecuteSql("update tb_service set serviceupdatetime=getdate() where id=@id", Par);
            return rev == 1;

        }
        public virtual DateTime? GetLastUpdateTime(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select serviceupdatetime from tb_service s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToDateTime(ds.Tables[0].Rows[0]["serviceupdatetime"]);
            }
            return null;
        }
        public virtual tb_service_model Get(DbConn PubConn, string servicenamespace)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@servicenamespace", servicenamespace));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_service s where s.servicenamespace=@servicenamespace");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
				return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

		public virtual tb_service_model CreateModel(DataRow dr)
        {
            var o = new tb_service_model();
			
			//
			if(dr.Table.Columns.Contains("id"))
			{
				o.id = dr["id"].Toint();
			}
			//服务名称
			if(dr.Table.Columns.Contains("servicename"))
			{
				o.servicename = dr["servicename"].Tostring();
			}
			//服务命名空间（服务唯一标示）
			if(dr.Table.Columns.Contains("servicenamespace"))
			{
				o.servicenamespace = dr["servicenamespace"].Tostring();
			}
			//服务类型, 1=thrift,2=wcf,3=http
			if(dr.Table.Columns.Contains("servicetype"))
			{
				o.servicetype = dr["servicetype"].ToByte();
			}
			//是否测试
			if(dr.Table.Columns.Contains("istest"))
			{
				o.istest = dr["istest"].Tobool();
			}
			//是否自动设置服务权重（根据服务性能，ping响应速度）
			if(dr.Table.Columns.Contains("isautoboostpercent"))
			{
				o.isautoboostpercent = dr["isautoboostpercent"].Tobool();
			}
			//协议版本号（当前）
			if(dr.Table.Columns.Contains("protocolversion"))
			{
				o.protocolversion = dr["protocolversion"].Todouble();
			}
			//协议更新时间
			if(dr.Table.Columns.Contains("protocolversionupdatetime"))
			{
				o.protocolversionupdatetime = dr["protocolversionupdatetime"].ToDateTime();
			}
			//服务变动更新时间（服务节点移除，添加需要更新这个时间）
			if(dr.Table.Columns.Contains("serviceupdatetime"))
			{
				o.serviceupdatetime = dr["serviceupdatetime"].ToDateTime();
			}
			//创建时间
			if(dr.Table.Columns.Contains("createtime"))
			{
				o.createtime = dr["createtime"].ToDateTime();
			}
			//备注
			if(dr.Table.Columns.Contains("remark"))
			{
				o.remark = dr["remark"].ToString();
			}
			return o;
        }
    }
}
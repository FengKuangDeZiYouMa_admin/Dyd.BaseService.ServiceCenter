﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dyd.BaseService.ServiceCenter.Web.Models
{
    public class ServiceViewModel
    {
        public tb_service tb_service { get; set; }
        public tb_protocolversion tb_protocolversion { get; set; }

    }
}